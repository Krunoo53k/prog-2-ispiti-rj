#include <stdio.h>
#include <stdlib.h>
#include "functions.h"



/*
    Napisati funkciju za izračunavanje faktorijela koristeći rekurziju. 
    
    Primjeri n! gdje su n jednaki 0, 1, 2, 3,...:

    0!=1; 1!=1; 2!=1*2; 3!=1*2*3, ....
*/

//. . . 

long factoriel(long n)
{
    if(n<=1)return 1;
    return n*factoriel(n-1);
}



/*
    Izračunaj nCr (#kombinacija) koristeći formulu iz teksta zadatka
*/
long izrac_ncr(int a, int b)
{
    return factoriel(a)/(factoriel(a-b)*factoriel(b));
}

/*
    Izračunaj nPr (#permutacija) koristeći formulu iz teksta zadatka
*/
long izrac_npr(int a, int b)
{
   return factoriel(a)/factoriel(a-b);
}



/*
  Funkcija za validiranja ulaznih podataka
  Vrati 1 ako su n i r granicama, inače vrati 0.
  Pripazi na to da je r (broj stavki uzetih iz skupa) manji ili jednak n (broja stavki u skupu).
*/
int provjera_podataka(int n, int r)
{
    if(n<0 || r<0) return 0;
    if(n>10 || r>10) return 0;
    if(n>=r)return 1;
    return 0;
}







/*
 NE MIJENAJATI!
 Funkcija za ispis i provjeru datoteka
*/
void ispisi_datoteku(char* outfile)
{

    char line[255];

    //otvori output
    FILE* fp = fopen(outfile, "r");
    if (fp == NULL)
        exit(-1);
    while ((fgets(line, 255, fp))) {
        printf("%s", line);
    }

    fclose(fp);
    remove(outfile);

}

FILE* zatvori_datoteku(FILE* pF) {

    if (fclose(pF) == 0) {
        printf("Oslobadjanje datoteke odradjeno\n");
    }

    return NULL;
}
