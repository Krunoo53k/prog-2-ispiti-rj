/* 

UVOD - Kalkulator kombinacija i permutacija

Kombinacija (nCr) je odabir r stavki iz skupa od n stavki bez da nas zanima redoslijed odabira.
-----------------------------------------------------------------------------------------

Primjer: Recimo da smo iz vrećice s 3 loptice crvene (C), zelene (Z) i plave (P) htjeli odabrati 2 loptice

Vrečica s kuglicam: (C)(Z)(P)

Postoje tri kombinacije ako zanemarimo redoslijed:
(C)(Z) (C)(P) (Z)(P)

Možemo izbrojati broj kombinacija bez ponavljanja pomoću formule nCr, gdje je n=3, a r=2.

	              n!        3!     6	
#kombinacija = -------- = ----- = --- = 3
               (n-r)!r!	  2!*1!	   2


Permutacija (nPr) je odabir r stavki iz skupa od n stavki pri čemu je bitan redoslijed kojim biramo naše stavke.
----------------------------------------------------------------------------------------------------------

Recimo da smo iz vrećice s 3 loptice crvene (C), zelene (Z) i plave (P) htjeli odabrati 2 loptice

Koliko ćemo jedinstvenih permutacija imati ako ne možemo ponoviti kuglice?

Postoji 6 permutacija nPr: (C)(Z)    (Z)(C)   (C)(P)   (P)(C)   (Z)(P)   (P)(Z)


                  n!       3!      3!     6
#permutacija = ------- = ------ = ---- = --- = 6
                (n-r)!   (3-2)!    1!     1




Zadatak:

Učitaj n (velicina skupa) i r (odabir) koji trebaju biti veći od -1 i manji od 10
Otvori datoteku za ispis pod imenom output.txt te za svaki par brojeva izračunaj #kombinacija i #permutacija.
Ako učitani podatak ne zadovoljava zadane uvjete, ispiši "Nevaljani Podaci n r" na ekran i preskoči računanje, gdje su n i r ulazni podaci.
Koristi funkciju zatvori_datoteku umjesto fclose()
Rezultate iz datoteke output.txt će se biti ispisana na standardnom izlazu s funkcijom ispisi_datoteku. 
To je zadnja funkcija programa koju nu treba mijenjati.

Zamjeni u kodu sve komentare "//. . ."" s vlastim kodom  

*/

#include <stdio.h>
#include <stdlib.h>
#include "functions.h"



int main()
{
    
    // definicije vasih lokalnih varijabli
    // . . .
    int n,r,npr,ncr;
    // predefinirane lokalne varijabli
    FILE* fpo;
    char* outfile = "output.txt";


    // Ucitaj broj elemenata skupa i broj odabranih stavki. Format za učitavanje je "%d%d#
    // Prvi broj u liniji je broj elemenata skupa, a drugi broj je broj odabranih elemenata
    scanf("%d%d#",&n,&r);
    printf("N=%d,R=%d\n",n,r);
    // . . .
    
    
    // Izracunaj permutacije i kombinacije
    npr=izrac_npr(n,r);
    ncr=izrac_ncr(n,r);
    // . . .
    
    // Ispisi neispravne linije na standardni izlaz u formatu
    // "Neispravni podaci: %d %d\n"
    if(!provjera_podataka(n,r))
        printf("Neispravni podaci: %d %d\n",n,r);
    // . . .
    
    // Ispisi rezultate u datoteku outfile u formatu (prvi broj je nPr, drugi broj je nCr)
    // "%d %d\n"
    fpo=fopen(outfile, "w");
    fprintf(fpo, "%d %d\n",npr,ncr);
    // . . .
   


    //Zatvori tokove iz datoteka koristeci funkciju iz predloska      
    zatvori_datoteku(fpo);

    //fpo
    // . . .
    
    
    //NE MIJENJAJ
    ispisi_datoteku(outfile);
    return 0;
}