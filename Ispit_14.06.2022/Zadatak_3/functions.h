#ifndef HEADER_FILE
#define HEADER_FILE

	// NAPISATI ove funkcije
	long izrac_npr(int, int);         //Izračunaj nCr(#kombinacija) koristeći formulu iz teksta zadatka
	long izrac_ncr(int, int);         //Izračunaj nPr (#permutacija) koristeći formulu iz teksta zadatka
	int provjera_podataka(int, int);  //Funkcija za validiranja ulaznih podataka


	//NE MIJENJATI *********************************************************************************
	void ispisi_datoteku();          //Funkcija za ispis datoteke na standardni izlaz - zadnja komanda u funkciji main()
	FILE* zatvori_datoteku(FILE*);   //Funkcija za zatvaranje toka (file stream) - Koristi ovu funkciju umjesto fclose()

#endif //HEADER_FILE