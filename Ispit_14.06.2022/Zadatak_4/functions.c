#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"


/*
Napisati funkciju readId(), pomoću koje se dohvati id studenta. Funkcija ne vraća vrijednost,
a za argument prima memorijsku adresu studenta.
Napisati funkciju readName(), pomocu koje se treba dohvatiti ime i prezime.
Možete iskoristiti mogućnost što funkcija scanf() prekida kada se naidje na prazninu i na taj
način dohvatiti posebno ime i prezime.
Funkcija readName() ne vraća vrijednost, a za argument prima memorijsku adresu studenta.
Napisati pomoćnu funkciju allocateString() čija je jedina zadaća dinamički zauzeti meoriju za
string određene duljuni, te vratiti referencu na zauzeti dio memorije.
Funkcija allocaeString() za argument prima ukupan broj elemenata plus jedan više za '\0', a
vraća memorijsku adresu početka uspješno zauzetog polja znakova.
*/

static void readId(STUDENT* node) {

	int id;
	fscanf(stream, "%d ", &id);
	node->id = id;
}

static char* allocateString(int n) {

	// dovršiti implemenetaciju
	char* tmp = (char*)malloc(sizeof(char) * n);
	if (tmp == NULL) {
		perror("Creating field for name");
		return NULL;
	}
	else {
		printf("Memory successfully allocated!\n");
		return tmp;
	}

}

static void readName(STUDENT* node) {

	char tmpField[50] = { 0 };
	// dohvatiti ime u privremeno polje
	fscanf(stream, "%s", tmpField);
	printf("Scanf has read: %s\n",&tmpField);
	node->firstName = allocateString(strlen(tmpField));
	strcpy(node->firstName, tmpField);

	// dohvatiti prezime u privremeno polje
	fscanf(stream, "%s", tmpField);
	printf("Scanf has read: %s\n",&tmpField);
	node->lastName = allocateString(strlen(tmpField));
	strcpy(node->lastName, tmpField);
}

/*
Za oslobađanje memorije od bilo kojeg tipa podatka za koji se dinamički zauzela memorija
potrebno je pozvati funkciju freeUpSpace(), koja ništa ne vraća, a za argument prima referencu
na dinamički zauzetu memoriju.
*/

static void freeUpSpace(void* memoryToBeFree) {

	free(memoryToBeFree);

	memoryToBeFree = NULL;

	if (memoryToBeFree == NULL) {
		printf("Memory successfully freed!\n");
	}
}

/*
Potrebno je kreirati jednostruko povezani popis pomoću funkcije createStudentList().
Funkcija ne prima argument, a vraća memorijsku adresu početka popisa.
*/

STUDENT* createStudentList(void) {

	// dovršiti implementaciju
	STUDENT* headNode = (STUDENT*)malloc(sizeof(STUDENT));
	if (headNode == NULL) {
		perror("Create head node");
		return NULL;
	}
	headNode->nextStudent = NULL;
	//pozvati funkcije za čitanje id-a, imena i prezimena
	readId(headNode);
	readName(headNode);

	return headNode;
}

/*
Dodati cetiri studenta na pocetak povezano popisa pomoću funkcije insertStudentToList().
Funkcija prima memorijsku adresu gdje počinje povezani popis,
a vraća memorijsku adresu novog početka povezanog popisa.
*/

STUDENT* insertStudentToList(STUDENT* headNode) {

	// dovršiti implementaciju
	STUDENT* oldHeadNode = headNode;
	STUDENT* newHeadNode = NULL;
	newHeadNode=(STUDENT*)malloc(sizeof(STUDENT));
	if (newHeadNode == NULL) {
		perror("Create new head node");
		return headNode;
	}
	newHeadNode->nextStudent = oldHeadNode;
	readId(newHeadNode);
	readName(newHeadNode);
		//pozvati funkcije za čitanje id-a, imena i prezimena

	return newHeadNode;
}

/*
Ispisati informacije o svakom studentu unutar povezanog popisa, ujedno i koliko ima elemenata
u povezanom popisu pomoću funkcije printStudentList().
Funkcija vraća broj elemenata u povezanom popisu, a prima memorijsku adresu gdje počinje
povezani popis.
*/

int printStudentList(STUDENT* traverseNode) {

	int counter = 0;

	// dovršiti implementaciju
	if (traverseNode == NULL) {
		return -1;
	}
	else {
		while (traverseNode) {
			counter++;
			printf("Node number: %d\tid: %d\tStudent name: %s\tStudent last name: %s\n", counter, traverseNode->id, traverseNode->firstName, traverseNode->lastName);
			traverseNode = traverseNode->nextStudent;
		}
	}
	return counter;
}

/*
Izbrisati studenta kojega se prvi puta kreiralo i to tako da ga se potraži preko id-a.
Brisanje studenta implementirati pomoću funkcije deleteStudentFromList(), a pretragu studenta
omogućiti pomoću funkcije searchStudent();
Funkcija deleteStudentFromList() prima memorijsku adresu pokazivača unutar kojeg je spremljena
memorijska adresa početka povezanog popisa i prima još memorijsku adresu studenta koji se treba
obrisati. Funkcija vraća 0 ako je uspješno odradila posao, a -1 ako je brisanje bilo neuspješno
Funkcija searchStudent() prima memorijsku adresu gdje počinje povezani popis i id studenta,
a vraća memorijsku adresu pronađenog studenta ili NULL memorijsku adresu u slučaju da ne može
pronaći studenta.
*/

STUDENT* searchStudent(STUDENT* traverseNode, int id) {

	// dovršiti implementaciju
	while (traverseNode)
	{
		if (traverseNode->id == id)
			return traverseNode;
		traverseNode = traverseNode->nextStudent;
	}
	return NULL;
}

int deleteStudentFromList(STUDENT** headNode, STUDENT* student) {

	if (student == NULL) {
		return -1;
	}

	STUDENT* traverseNode;

	for (traverseNode = (*headNode); traverseNode->nextStudent != NULL; traverseNode = traverseNode->nextStudent) {

		if (traverseNode->nextStudent == student)
		{
			traverseNode->nextStudent = student->nextStudent;
			freeUpSpace(student->firstName);
			freeUpSpace(student->lastName);
			freeUpSpace(student);
			return 0;
		}

	}

	return -1;
}

/*
Na kraju je potrebno obrisati cijeli povezani popis pomoću funkcije deleteWholeStudentList().
Funkcija prima memorijsku adresu gdje počinje povezani popis, a vraća NULL memorijku adresu
koja se upisuje u pokazivač u main()-u.
*/

STUDENT* deleteWholeStudentList(STUDENT* traverseNode) {

	while (traverseNode != NULL) {

		STUDENT* deleteNode = traverseNode;
		traverseNode = traverseNode->nextStudent;
		freeUpSpace(deleteNode->firstName);
		freeUpSpace(deleteNode->lastName);
		freeUpSpace(deleteNode);

	}

	return NULL;
}

