#include <stdlib.h>
#include <stdio.h>
#include "functions.h"

int upisBrojaNatjecatelja()
{

    int n;

    printf("Koliko natjecatelja zelis u popisu?\n");
    scanf("%d", &n);

    return n;
}

NATJECATELJ *kreiranjePopisaNatjecatelja()
{

    // dovrsi implementaciju---------napisano
    NATJECATELJ *glavniNatjecatelj = (NATJECATELJ *)calloc(1, sizeof(NATJECATELJ));
    if (glavniNatjecatelj == NULL)
    {
        perror("Kreiranje");
        return NULL;
    }
    else
    {
        glavniNatjecatelj->ime = (char *)malloc(50);
        if (glavniNatjecatelj->ime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        glavniNatjecatelj->prezime = (char *)malloc(50);
        if (glavniNatjecatelj->prezime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        glavniNatjecatelj->natjecanje = (NATJECANJE *)malloc(sizeof(NATJECANJE));
        if (glavniNatjecatelj->natjecanje == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        glavniNatjecatelj->natjecanje->imeNatjecanja = (char *)malloc(50);
        if (glavniNatjecatelj->natjecanje->imeNatjecanja == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }

        glavniNatjecatelj->sljedeciNatjecatelj = NULL;
    }

    return glavniNatjecatelj;
}

NATJECATELJ *kreirajNovogNatjecatelja(NATJECATELJ *glavniNatjecatelj)
{

    // dovrsi implementaciju----napisano
    NATJECATELJ *noviNatjecatelj = (NATJECATELJ *)calloc(1, sizeof(NATJECATELJ));
    if (noviNatjecatelj == NULL)
    {
        perror("Kreiranje");
        return NULL;
    }
    else
    {
        noviNatjecatelj->ime = (char *)malloc(50);
        if (noviNatjecatelj == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        noviNatjecatelj->ime = (char *)malloc(50);
        if (noviNatjecatelj->ime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        noviNatjecatelj->prezime = (char *)malloc(50);
        if (noviNatjecatelj->prezime == NULL)
        {
            perror("KreiranjeS");
            return NULL;
        }
        noviNatjecatelj->natjecanje = (NATJECANJE *)malloc(sizeof(NATJECANJE));
        if (noviNatjecatelj->natjecanje == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        noviNatjecatelj->natjecanje->imeNatjecanja = (char *)malloc(50);
        if (noviNatjecatelj->natjecanje->imeNatjecanja == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        noviNatjecatelj->sljedeciNatjecatelj = glavniNatjecatelj;
    }

    return noviNatjecatelj;
}

void unosNatjecatelja(NATJECATELJ *natjecatelj)
{

    // dovrsi implementaciju------napisano
    scanf("%s", natjecatelj->ime);
    scanf("%s", natjecatelj->prezime);
    scanf("%d", &natjecatelj->broj);
    scanf("%s", natjecatelj->natjecanje->imeNatjecanja);
    scanf("%d", &natjecatelj->natjecanje->kategorija);

    // zauzmi malo za ime i provjera
    // zauzmi malo za prezime i provjera
    // nesto dohvati s ulaza
    // zauzmi malo za natjecanje i provjera
    // zauzmi malo za imeNatjecanja i provjera
    // nesto dohvati s ulaza
}

void ispisNatjecatelja(NATJECATELJ *natjecateljKojiObilazi)
{

    // dovrsi implementaciju ------napisano
    if (natjecateljKojiObilazi == NULL)
    {
        exit(EXIT_FAILURE);
    }
    else
    {
        while (natjecateljKojiObilazi)
        {
            printf("Prezime natjecatelja: %s\nNatjecateljev broj: %d\nKategorija natjecanja: %d\n", natjecateljKojiObilazi->prezime, natjecateljKojiObilazi->broj,
                   natjecateljKojiObilazi->natjecanje->kategorija);
            natjecateljKojiObilazi = natjecateljKojiObilazi->sljedeciNatjecatelj;
        }
    }
}

int brojNatjecateljaPretraga()
{

    int x;

    printf("\nPod kojim brojem zelis natjecatelja?\n");
    scanf("%d", &x);

    return x;
}

NATJECATELJ *pretragaNatjecatelja(NATJECATELJ *glavniNatjecatelj, int brojNatjecatelja)
{

    // dovrsi implementaciju-----------napisano
    if (glavniNatjecatelj == NULL)
    {
        return NULL;
    }
    else
    {
        while (glavniNatjecatelj)
        {
            if (glavniNatjecatelj->broj == brojNatjecatelja)
                return glavniNatjecatelj;
            glavniNatjecatelj = glavniNatjecatelj->sljedeciNatjecatelj;
        }
    }

    return NULL;
}

void ispisZeljenogNatjecatelja(NATJECATELJ *natjecateljKojiObilazi)
{

    // dovrsi implemenatciju-----napisano
    if (natjecateljKojiObilazi != NULL)
    {
        printf("Prezime natjecatelja: %s\nNatjecateljev broj: %d\nNatjecanje u: %s\nKategorija natjecanja: %d\n", natjecateljKojiObilazi->prezime, natjecateljKojiObilazi->broj,
               natjecateljKojiObilazi->natjecanje->imeNatjecanja, natjecateljKojiObilazi->natjecanje->kategorija);
    }
}

NATJECATELJ *oslobadjanjeNatjecatelja(NATJECATELJ *glavniNatjecatelj)
{
    // dovrsi implementaciju
    // brisi sve za sto se din. zauzimala memorija, a ima toga!!!
    NATJECATELJ *tmp = NULL;
    tmp = glavniNatjecatelj;
    glavniNatjecatelj = glavniNatjecatelj->sljedeciNatjecatelj;
    free(tmp->natjecanje->imeNatjecanja);
    free(tmp->natjecanje);
    free(tmp->prezime);
    free(tmp->ime);
    free(tmp);
}
