#ifndef FUNCTIONS_H
#define FUNCTIONS_H

typedef struct natjecanje {
	char *imeNatjecanja; 
	int kategorija;
}NATJECANJE;

typedef struct natjecatelj{
	char *ime;
	char *prezime;
	int broj;
	NATJECANJE *natjecanje;
	struct natjecatelj* sljedeciNatjecatelj;
}NATJECATELJ;


int upisBrojaNatjecatelja();
NATJECATELJ* kreiranjePopisaNatjecatelja();
NATJECATELJ* kreirajNovogNatjecatelja(NATJECATELJ*);
void unosNatjecatelja(NATJECATELJ*);
void ispisNatjecatelja(NATJECATELJ*);
int brojNatjecateljaPretraga();
NATJECATELJ* pretragaNatjecatelja(NATJECATELJ*, int );
void ispisZeljenogNatjecatelja(NATJECATELJ*);
NATJECATELJ* oslobadjanjeNatjecatelja(NATJECATELJ*);

#endif //FUNCTIONS_H