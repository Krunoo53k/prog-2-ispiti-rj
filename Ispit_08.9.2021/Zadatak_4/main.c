/*
Napisati C program koji koristi jednostruko povezani popis za strukturu NATJECATELJ.

Struktura ima clanove: pokazivace na ime i prezime natjecatelja,
cjelobrojnu varijablu natjecateljev broj i
pokazivac na strukturu NATJECANJE, koja ima clanove:
pokazivac na ime natjecanja, jedni od mogucih: [nogomet, rukomet, gimnastika, kosarka] i
cjelobrojnu varijablu kategorije, jednu od mogućih: [1 - senior, 2 - junior, 3 - kadet]

Napisati odgovarajuce funkcije za kreiranje povezanog popisa i ubacivanje novog clana:
kreiranjePopisaNatjecatelja(), kreirajNovogNatjecatelja().

Napisati funkciju unosNatjecatelja() s kojom se unose svi podaci u clanove strukture i pozvati
funkciju na odgovarajucem mjestu u programu.

Napisati funkciju ispisNatjecatelja() koja ispisuje povezani popis u obliku:
Prezime natjecatelja:
Natjecateljev broj:
Kategorija natjecanja:

Napisati funkciju pretragaPopisa() za pretrazivanje povezanog popisa po natjecateljevom broju:

Napisati funkciju ispisZeljenogNatjecatelja() koja ispisuje pronadjenog natjecatelja prethodnom funkcijom.

Prezime natjecatelja:
Natjecateljev broj:
Natjecanje u:
Kategorija natjecanja:

Napisati funkciju oslobadjanjeNatjecatelja() koja oslobadja dinamicki zauzetu memoriju za povezani popis.

                                    *****************************
                                    *****************************
                                    *****IZNIMNO UPOZORENJE******
                                    ***********SLIJEDI***********
                                    *****************************
                                    *****************************
                                    **********NAPOMENA***********
                                    *****************************
                                    *****************************
                                    *****IZNIMNO UPOZORENJE******
                                    ***********SLIJEDI***********
                                    *****************************
                                    *****************************
                                        
Iako vam testovi mogu imati 100/100, pregledavat će se jeste li radili provjeru nakon svakog dinamičko zauzimanja memorije kao i oslobađanje memorije.
Ako bi se dogodilo da to niste odradili, npr. provjera nije odrađena u prvom dijelu, 
tada mozete dobiti pola bodova ako vam drugi dio zadatka radi, tj. ako ste ujedno oslobodili i memoriju. Vrijedi i obrnuto, ako imate provjere,
a niste sve oslobodili, tada možete dobiti samo pola bodova. Molim vas da obratite pozornost na ove zahtjeve!!!
*/

#include <stdio.h>
#include "functions.h"

int main() {

	int n, i, x;

	NATJECATELJ* glavniNatjecatelj = NULL;
	NATJECATELJ* zeljeniNatjecatelj = NULL;

	n = upisBrojaNatjecatelja();
	glavniNatjecatelj = kreiranjePopisaNatjecatelja();
	unosNatjecatelja(glavniNatjecatelj);

	for (i = 0; i < n - 1; i++)
	{
		glavniNatjecatelj = kreirajNovogNatjecatelja(glavniNatjecatelj);
		unosNatjecatelja(glavniNatjecatelj);
	}

	ispisNatjecatelja(glavniNatjecatelj);
	x = brojNatjecateljaPretraga();
	zeljeniNatjecatelj = pretragaNatjecatelja(glavniNatjecatelj, x);

	printf("REZULTATI:\n");
	if (zeljeniNatjecatelj == NULL) {
		printf("Natjecatelj pod tim brojem nije pronadjen!");
	}
	else ispisZeljenogNatjecatelja(zeljeniNatjecatelj);

	glavniNatjecatelj = oslobadjanjeNatjecatelja(glavniNatjecatelj);
	if (glavniNatjecatelj == NULL)
		printf("Oslobodjen cvor!");

	return 0;
}