#ifndef HEADER_H
#define HEADER_H

typedef struct natjecatelj {
	char *ime;
	char *prezime;
	int broj;
	struct natjecanje *Natjecanje;
	struct natjecatelj* noviCvor;
}NATJECATELJ;

typedef struct natjecanje {
	char *imeNatjecanja;
	int kategorija;
}NATJECANJE;

int UpisBrojNatjecatelja();
int BrojNatjecateljaPretraga();
void UnosNatjecatelja(NATJECATELJ*);
void IspisNatjecatelja(NATJECATELJ* );
NATJECATELJ* kreiranjePopisa();
NATJECATELJ* ubaciNoviCvor(NATJECATELJ*);
NATJECATELJ* PretragaPopisa(NATJECATELJ*, int );
NATJECATELJ* OslobadjanjeNatjecatelja(NATJECATELJ* );
void IspisZeljenogNatjecatelja(NATJECATELJ*);
#endif