/*Napisati C program koji koristi jednostruko povezani popis za strukturu NATJECATELJ.

	Struktura ima clanove: pokazivace na ime i prezime natjecatelja,
	cjelobrojnu varijablu natjecateljev broj i
	i pokazivac na strukturu NATJECANJE, koja ima clanove:

	pokazivac na ime natjecanja [nogomet, rukomet, gimnastika, kosarka]
	i cjelobrojna varijabla kategorije [1- senior, 2 - junior, 3 - kadet]


Napisati odgovarajuce funkcije za kreiranje povezanog popisa i ubacivanje novog clana:
KreiranjePopisa(), UnesiNoviCvor().

Napisati funkciju UnosNatjecatelja() s kojom se unose svi podaci u clanove strukture i pozvati
funkciju na odgovarajucem mjestu u programu.


Napisati funkciju IspisNatjecatelja() koja ispisuje povezani popis u obliku:

Prezime natjecatelja:
Natjecateljev broj:
Kategorija natjecanja:


Napisati funkciju PretragaPopisa() za pretrazivanje povezanog popisa po natjecateljevom broju:

Napisati funkciju IspisZeljenogNatjecatelja() koja ispisuje pronadjenog natjecatelja prethodnom funkcijom.

Prezime natjecatelja:
Natjecateljev broj:
Natjecanje u:
Kategorija natjecanja:

Napisati funkciju OslobadjanjeNatjecatelja() koja oslobadja dinamicki zauzetu memoriju
za povezani popis.

U potpunosti rukovati memorijom za sve varijable za koje je to nuzno.
*/
#include <stdio.h>
#include "Funkcije.h"

int main()
{

	int n, i, x;

	NATJECATELJ *headNode = NULL;
	NATJECATELJ *ZeljeniNatjecatelj = NULL;

	n = UpisBrojNatjecatelja();
	headNode = kreiranjePopisa();

	for (i = 0; i < n - 1; i++)
	{
		headNode = ubaciNoviCvor(headNode);
	}

	IspisNatjecatelja(headNode);
	x = BrojNatjecateljaPretraga();
	ZeljeniNatjecatelj = PretragaPopisa(headNode, x);

	printf("REZULTATI:\n");
	if (ZeljeniNatjecatelj == NULL)
	{
		printf("Natjecatelj pod tim brojem nije pronadjen!");
	}
	else
		IspisZeljenogNatjecatelja(ZeljeniNatjecatelj);

	headNode = OslobadjanjeNatjecatelja(headNode);
	if (headNode == NULL)
		printf("Oslobodjen cvor!");

	return 0;
}
