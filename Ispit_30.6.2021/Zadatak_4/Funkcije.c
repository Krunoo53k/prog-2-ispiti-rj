#include <stdlib.h>
#include <stdio.h>
#include "Funkcije.h"

int UpisBrojNatjecatelja()
{
    int n;
    printf("Koliko natjecatelja zelis u popisu?\n");
    scanf("%d", &n);

    return n;
}

int BrojNatjecateljaPretraga()
{
    int x;
    printf("\nNatjecatelja pod kojim brojem zelis?\n");
    scanf("%d", &x);
    return x;
}
void UnosNatjecatelja(NATJECATELJ *headNode)
{

    // dovrsi implementaciju
    scanf("%s", headNode->ime);
    scanf("%s", headNode->prezime);
    scanf("%d", &headNode->broj);
    scanf("%s", headNode->Natjecanje->imeNatjecanja);
    scanf("%d", &headNode->Natjecanje->kategorija);
}

void IspisNatjecatelja(NATJECATELJ *traverseNode)
{

    // dovrsi implementaciju
    if (traverseNode == NULL)
    {
        exit(EXIT_FAILURE);
    }
    else
    {
        while (traverseNode)
        {
            printf("Prezime natjecatelja: %s\nNatjecateljev broj: %d\n Kategorija natjecanja: %d\n", traverseNode->prezime, traverseNode->broj, traverseNode->Natjecanje->kategorija);
            traverseNode=traverseNode->noviCvor;
        }
    }
}

void IspisZeljenogNatjecatelja(NATJECATELJ *traverseNode)
{

    // dovrsi implementaciju
    if (traverseNode != NULL)
    {
        printf("Prezime natjecatelja: %s\nNatjecateljev broj: %d\n Natjecanje u: %s\n Kategorija natjecanja: %d\n", traverseNode->prezime, traverseNode->broj, traverseNode->Natjecanje->imeNatjecanja,
               traverseNode->Natjecanje->kategorija);
    }
}

NATJECATELJ *kreiranjePopisa()
{

    // dovrsi implementaciju
    NATJECATELJ *headNode = (NATJECATELJ *)calloc(1, sizeof(NATJECATELJ));
    if (headNode == NULL)
    {
        perror("Kreiranje");
        return NULL;
    }
    else
    {
        headNode->ime = (char *)malloc(50);
        if (headNode->ime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        headNode->prezime = (char *)malloc(50);
        if (headNode->prezime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        headNode->Natjecanje = (NATJECANJE *)malloc(sizeof(NATJECANJE));
        if (headNode->Natjecanje == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        headNode->Natjecanje->imeNatjecanja = (char *)malloc(50);
        if (headNode->Natjecanje->imeNatjecanja == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        headNode->noviCvor = NULL;
        UnosNatjecatelja(headNode);
    }
    return headNode;
}

NATJECATELJ *ubaciNoviCvor(NATJECATELJ *headNode)
{

    // dovrsi implementaciju
    NATJECATELJ *newHeadNode = (NATJECATELJ *)calloc(1, sizeof(NATJECATELJ));
    if (newHeadNode == NULL)
    {
        perror("Kreiranje");
        return NULL;
    }
    else
    {
        newHeadNode->ime = (char *)malloc(50);
        if (newHeadNode->ime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        newHeadNode->prezime = (char *)malloc(50);
        if (newHeadNode->prezime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        newHeadNode->Natjecanje = (NATJECANJE *)malloc(sizeof(NATJECANJE));
        if (newHeadNode->Natjecanje == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        newHeadNode->Natjecanje->imeNatjecanja = (char *)malloc(50);
        if (newHeadNode->Natjecanje->imeNatjecanja == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        UnosNatjecatelja(newHeadNode);
        newHeadNode->noviCvor = headNode;
    }
    return newHeadNode;
}

NATJECATELJ *OslobadjanjeNatjecatelja(NATJECATELJ *glavniCvor)
{
    NATJECATELJ *tmp = NULL;

    // dovrsi implementaciju
    tmp = glavniCvor;
    glavniCvor = glavniCvor->noviCvor;
    free(tmp->Natjecanje->imeNatjecanja);
    free(tmp->Natjecanje);
    free(tmp->prezime);
    free(tmp->ime);
    free(tmp);
}

NATJECATELJ *PretragaPopisa(NATJECATELJ *headNode, int x)
{

    // dovrsi implementaciju
    if (headNode == NULL)
    {
        return NULL;
    }
    else
    {
        while (headNode)
        {
            if (headNode->broj == x)
                return headNode;
            headNode = headNode->noviCvor;
        }
    }
    return NULL;
}
