#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "functions.h"

char* porukaZaDatoteku(char* nacinRada) {

	char* poruke[] = { "Citanje", "Pisanje", "Dodavanje", "Citanje i pisanje", "Pisanje i citanje", "Dodavanje i citanje", "Ne definirani nacin rada" };

	if (!strcmp("r", nacinRada)) {
		return poruke[0];
	}
	else if (!strcmp("w", nacinRada)) {
		return poruke[1];
	}
	else if (!strcmp("a", nacinRada)) {
		return poruke[2];
	}
	else if (!strcmp("r+", nacinRada)) {
		return poruke[3];
	}
	else if (!strcmp("w+", nacinRada)) {
		return poruke[4];
	}
	else if (!strcmp("a+", nacinRada)) {
		return poruke[5];
	}else{
		return poruke[6];
	}
}

void provjeraDatoteke(FILE* pF, char* nacinRada) {

	if (pF == NULL) {
		perror(porukaZaDatoteku(nacinRada));
		exit(EXIT_SUCCESS);
    }else{
	    printf("Provjera datoteke odradjena\n");
		//idemo
	}
}

FILE* radSDatotekomManipuliranje(char* imeDatoteke, char* nacinRada) {

	FILE* pF = fopen(imeDatoteke, nacinRada);
	return pF;
}

FILE* radSDatotekomOslobadjanje(FILE* pF) {
    
    if(fclose(pF) == 0){
        printf("Oslobadjanje datoteke odradjeno\n");
    }

	return NULL;
}

int ucitavanjeElemenata(FILE* pF) {

	int vrijednost_elementa;
	fscanf(pF,"%d i",&vrijednost_elementa);
	printf("ucitan je broj: %d\n",vrijednost_elementa);
	return vrijednost_elementa;
}

void provjeraZauzimanjaPolja(KOMPLEKS* polje) {
    
    if (polje == NULL) {
		perror("Zauzimanje polja");
		exit(EXIT_FAILURE);
	}else{
	    printf("Provjera zauzimanja odradjena\n");
	}
}

KOMPLEKS* zauzimanjePoljaKompleksnih(int brojKompleksnih) {

	KOMPLEKS* polje = (KOMPLEKS*)calloc(brojKompleksnih, sizeof(KOMPLEKS));
	
	provjeraZauzimanjaPolja(polje);
	
	return polje;
}

void ucitavanjeKompleksnihBrojeva(FILE* pF, KOMPLEKS* polje, int brojKompleksnih) {

	rewind(pF);

	//dovrsiti implementaciju
	for(int i=0; i<brojKompleksnih;i++)
	{
		(polje+i)->re=ucitavanjeElemenata(pF);
		(polje+i)->im=ucitavanjeElemenata(pF);
	}
}

void modulKompleksnogBroja(KOMPLEKS* polje, int brojKompleksnih) {

	float module;
	for(int i=0;i<brojKompleksnih;i++)
	{

		//ovaj dio jede govna nije mi jasno uopce zasto, cak i ako ga kao i oznacim, svejedno stavi da su sve jedinice??
		float module = sqrt((polje+i)->im*(polje+i)->im+(polje+i)->re*(polje+i)->re);
		//petlja se izvrti fino, cak i ako printas modul sve fino izbacuje, al samo iz nekog razloga ne zeli dat vrijednost i amen
		//zasto kod 10 linija iznad fino funkcionira, ali ovo ne??
		(polje+i)->modul=module;
		printf("Modul je: %f\n",(polje+i)->modul);
		//printf isto dobro isprinta, znaci sve bi trebalo bit u redu al nije...
		
	}
}

void ispisNajvecegModula(KOMPLEKS* najveci, int indeks){
    
    
    if (najveci->im < 0) {

		printf("index: %d complex number: %d - %d i module: %f\n", indeks, najveci->re, najveci->im*-1, najveci->modul);
	}
	else {
		printf("index: %d complex number: %d + %d i module: %f\n", indeks, najveci->re, najveci->im, najveci->modul);
	}
}

void najveciModul(KOMPLEKS* polje, int brojKompleksnih) {

	KOMPLEKS* najveci = polje;
	int j = 0;
	for(int i = 0;i<brojKompleksnih;i++)
	{
		if((polje+i)->modul>=najveci->modul)
		{
			najveci=polje+i;
			j=i;
		}
	}
    ispisNajvecegModula(polje+j, j);
}

void zamjena(KOMPLEKS* veci, KOMPLEKS* manji) {
	KOMPLEKS temp;
	temp = *veci;
	*veci = *manji;
	*manji = temp;
}

void sortiranjeKompleksnihBrojeva(KOMPLEKS* polje, int brojKompleksnih) {
    	
    int i, j, min;
    for (i = 0; i < brojKompleksnih-1; i++) {
        min = i;
        for (j = i+1; j < brojKompleksnih; j++)
            if (polje[j].modul < polje[min].modul) 
                min = j;
        zamjena(polje + i, polje + min);
	}
}

void ispisKompleksnihBrojeva(KOMPLEKS* polje, int brojKompleksnih) {

	int i;

	for (i = 0; i < brojKompleksnih; i++)
	{	//zadatak ne trazi da ispises module, ali ja jesam jer NE FUNKCIONIRA
		if ((polje + i)->im < 0) {
			printf("index: %d complex number: %d - %d i module: %f\n", i, (polje + i)->re, (polje + i)->im * -1, (polje+1)->modul);
		}
		else {
			printf("index: %d complex number: %d + %d i module: %f\n", i, (polje + i)->re, (polje + i)->im,(polje+1)->modul);
		}
	}
}

KOMPLEKS* oslobadjanjePoljaKompleksnihBrojeva(KOMPLEKS* polje) {

	free(polje);
	
	polje = NULL;
	
	if(polje == NULL){
	    printf("Oslobadjanje polja odradjeno\n");
	}

	return NULL;
}