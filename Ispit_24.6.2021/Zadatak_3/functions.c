#include <stdio.h>
#include <stdlib.h>
#include "functions.h"


float vTezina[4] = { .40, .15, .20, .25 };  // vektor koji opisuje težinu koja se daje 
                                            // bodovima iz različitih aktivnosti
/*
    Gotova funkcija - NE MIJENJATI ----------------------------------------
    Ocjena (cijeli broj od 1-5)   (ocjena se racuna: 0.00-50.00 bodova = 1
                                                >50.00-65.00 bodova = 2
                                                >65.00-80.00 bodova = 3
                                                >80.00-90.00 bodova = 4
                                                >90.00 bodova = 5       )
*/
int ocjena(float bodovi)
{
    if (bodovi >= 0.0 && bodovi <= 50.0)
        return 1;
    if (bodovi > 50.0 && bodovi <= 65.0)
        return 2;
    if (bodovi > 65.0 && bodovi <= 80.0)
        return 3;
    if (bodovi > 80.0 && bodovi <= 90.0)
        return 4;
    if (bodovi > 90.0 && bodovi <= 100.0)
        return 5;
    else
        return 0;
}

/* ******************************************************************************************************************
    Funkciju koristi polje struktura studenskih ocjena za jedan predmet te  
    racuna za svakog studenta ukupni broj bodova zbrajanjem bodova pomnoženog s težinom za svaki element ocjene
    Ukupni bodovi se pretvaraju u ocjenu koristeći gotovu funkciju ocjena i upisuju se u strukturu.
*/

void izrac_ocjene(int brojStudenata, POCJENA* Prog)
{
    float bodovi = 0;
    // algoritam za izračunavanje ukupnog broja bodova i ocjene
    for(int i=0;i<brojStudenata;i++)
    {
        bodovi=0;
        bodovi+=Prog[i].kv*vTezina[1]+Prog[i].lv*vTezina[0]
            +Prog[i].predavanja*vTezina[2]+Prog[i].projekt*vTezina[3];
        Prog[i].ocjena=ocjena(bodovi);
    }
    // ...

}

/*
  Funkcija za validiranja bodova
  
*/
int provjera_bodova(int x)
{
    
    // završi funkciju 
    // ...
    if(x<0 || x>100)
        return -1;
    return 0;
}

/*
  Funkcija za validiranja broja studenata
*/
int provjera_broja_studenata(int x)
{
    if(x>=31 || x<1)
        return -1;   
    return 0;
}


/*************************************************************************************************************************
 NE MIJENAJATI!
 Funkcija za ispis i provjeru datoteka
*/
void ispis_ocjene(int brojStudenata, POCJENA* prog)
{
    for (int i = 0; i < brojStudenata; i++)  // 
    {
        printf("%s, %s: %d\n", prog[i].prezime, prog[i].ime, prog[i].ocjena);
    }
}

void oslobodi(POCJENA* prog) {

    free(prog);
    printf("Oslobadjanje memorije odradjeno.\n");
}
