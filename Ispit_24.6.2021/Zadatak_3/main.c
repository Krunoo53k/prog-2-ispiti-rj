/* 

UVOD - Ukupna ocjena

Ocjena iz nekog predmeta sastoji se od 4 elementa koji svaki pridonosi s određenom težinom ukupnoj ocjeni. Pretpostavimo ovako definirane težine:

  Laboratorijske vježbe  40%
  Konstrukcijske vježbe  15%
  Predavanja             20%
  Projekt                25%

Bodovi studenata su uneseni u polje struktura gdje je pojedinačni student spremljen u strukturu sa slijedećim
elementima:
  Id (integer)
  Ime (20 znakova)
  Prezime (20 znakova)
  LV  (cijeli broj 1-100)
  KV  (cijeli broj 1-100)
  Predavanja  (cijeli broj 1-100)
  Projekt  (cijeli broj 1-100)
  Ocjena (cijeli broj od 1-5)   (ocjena se računa: 0.00-50.00 bodova   = 1
                                                   >50.00-65.00 bodova = 2
                                                   >65.00-80.00 bodova = 3
                                                   >80.00-90.00 bodova = 4
                                                   >90.00 bodova       = 5 )
Zadatak:

Izračunati i ispisati ukupnu ocjenu za svakog studenta.

Pazi na granice i tip zadanih podataka.
Učitaj broj studenata N, gdje je N prirodni broj manji od 31.
Učitaj podatke studenata u polje struktura veličine N. 
Zaustavi program i ispiši "Losi Podaci" ako podaci o studenti nisu valjani iz bilo kojeg razloga:
    -nedostaju bodovi (manjak informacije je označen s -1)
    -bodovi veći od 100 ili manji od 0
Dinamički zauzmi polje struktura.

Ukupan broj bodova po studentu je jednak:

      B(lv)*T(lv)+B(kv)*T(kv)+B(predavanja)*T(predavanja)+B(projekt)*T(projekt)  

gdje su B(x) bodovi a T(x) težina komponente x (lv|kv|predavanja|projekt)


Primjer: Svaki red sadrži bodove jednog studenta iz LV, KV, Pred., Proj.

    Težinski faktor za elemente ispita je zadan kao što je objašnjeno:

    .40     (Laboratorijske vježbe  40%)
    .15     (Konstrukcijske vježbe  15%)
    .20     (Predavanja             20%)
    .25     (Projekt                25%)

Koristeći rezultat za ukupni broj bodova izračunaj ocjenu i unesi je u strukturu za svakog studenta. 
Ispiši strukturu koja sadrži ocjenu na ekran koristeći gotovu funkciju ispis_studenata.
Za oslobadjanje zauzete memorije koristi gotvu funkciju oslobodi(...) deklariranu u functions.h.


*/

#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
#include <string.h>


int main()
{

    // za učitani broj studenata
    // zauzmi memoriju za polje struktura 
    int brojStudenata;
    POCJENA *studenti;
    scanf("%d",&brojStudenata);
    if(provjera_broja_studenata(brojStudenata)==0){
      studenti=malloc(brojStudenata*sizeof(brojStudenata));
    }
    else
      return -1;//valjda ja moram izać ak ne valja to?

    // . . . 


    // ucitaj valjane podatke
    // izadji iz programa ako podaci nisu valjani
    for(int i=0;i<brojStudenata;i++){
      int id, kv, lv, projekt, predavanja;
      char ime[20],prezime[20];
      scanf("%d",&studenti[i].id);
      scanf("%s %s",ime,prezime);
      //printf("%s %s\n",ime,prezime); S ovim printfom radi na mom kompu, bez njega dobije corrupt top size ??
      scanf("%d %d %d %d",&lv,&kv,&predavanja,&projekt);
      //odvratni dio koda
      if(provjera_bodova(lv)==-1)
        return -1;
      if(provjera_bodova(kv)==-1)
        return -1;
      if(provjera_bodova(predavanja)==-1)
        return -1;
      if(provjera_bodova(projekt)==-1)
        return -1;
      studenti[i].id=id;
      strncpy(studenti[i].ime,ime,20);
      strncpy(studenti[i].prezime,prezime,20);
      studenti[i].kv=kv;
      studenti[i].lv=lv;
      studenti[i].projekt=projekt;
      studenti[i].predavanja=predavanja;
    }
    // . . . 


    // zamjeni "..." s varijablama za broj studenata i strukturu ocjena 
    //  prema deklaraciji danoj u functions.h
    
    izrac_ocjene(brojStudenata, studenti);
    
    ispis_ocjene(brojStudenata, studenti);
    
    oslobodi(studenti);
       
    return 0;
}