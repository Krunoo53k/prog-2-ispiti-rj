#ifndef HEADER_FILE
#define HEADER_FILE

typedef struct PredmetOcjena
{
	int id;            
	char ime[20];
	char prezime[20];
	int lv;				// cijeli broj 1 - 100
	int kv;				// cijeli broj 1 - 100
	int predavanja;		// cijeli broj 1 - 100
	int projekt;       // cijeli broj 1 - 100
	int ocjena;         // cijeli broj od 1 - 5
}POCJENA;

	int provjera_bodova(int n);
	int provjera_broja_studenata(int n);
	void izrac_ocjene(int brojStudenata, POCJENA* prog);
	

	//NE MIJENJATI *********************************************************************************
	void ispis_ocjene(int, POCJENA*);    // Funkcija za ispis ocjene prije kraju funkcije main()
	void oslobodi(POCJENA*);            // Funkcija za oslobađanje memorije - Koristi ovu funkciju umjesto free()


#endif //HEADER_FILE