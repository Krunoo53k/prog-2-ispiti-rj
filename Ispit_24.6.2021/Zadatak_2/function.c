#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header.h"

int vratiNajboljeVrijeme(int minuta, int sekunda, int milisekunda, int trenutnoNajboljeVrijeme[3])
{
    if (minuta < trenutnoNajboljeVrijeme[0])
    {
        int novoNajboljeVrijeme[3] = {minuta, sekunda, milisekunda};
        return novoNajboljeVrijeme;
    }
    else if (minuta == trenutnoNajboljeVrijeme[0])
    {
        if (sekunda < trenutnoNajboljeVrijeme[1])
        {
            int novoNajboljeVrijeme[3] = {minuta, sekunda, milisekunda};
            return novoNajboljeVrijeme;
        }
    }
    else if (minuta == trenutnoNajboljeVrijeme[0] && sekunda == trenutnoNajboljeVrijeme[1])
    {
        if (milisekunda == trenutnoNajboljeVrijeme[2])
        {
            int novoNajboljeVrijeme[3] = {minuta, sekunda, milisekunda};
            return novoNajboljeVrijeme;
        }
    }
    else
        return trenutnoNajboljeVrijeme;
}
