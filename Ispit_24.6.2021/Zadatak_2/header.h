#ifndef FUNCTIONS_H
#define FUNCTIONS_H
/*2. Deklarirati strukturu plivacki_kup u koju će se spremiti ovakvi ulazni 
podaci: ime, prezime, godina rodjenja, klub, disciplina, duzina, ostvareno vrijeme.
	U program ulazi broj osoba koje se natjecu i nakon toga redom podaci o njima.*/
//deklaracija strukture
typedef struct Plivac
{         
	char ime[20];
	char prezime[20];
	int godRodenja;
	char imeKluba[20];
	char disciplina[20];
	int duzina;
    int minuta;
    int sekunda;
    int milisekunda;
}PLIVAC;



//deklaracije funkcija  
void unesiPodatke(PLIVAC *Plivac);
int vratiNajboljeVrijeme(int minuta, int sekunda, int milisekunda, int trenutnoNajboljeVrijeme[3]);
#endif