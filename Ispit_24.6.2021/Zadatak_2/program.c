#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"

/*2. Deklarirati strukturu plivacki_kup u koju će se spremiti ovakvi ulazni
podaci: ime, prezime, godina rodjenja, klub, disciplina, duzina, ostvareno vrijeme.
	U program ulazi broj osoba koje se natjecu i nakon toga redom podaci o njima.

	Primjer ulaznih podataka
	 3
	Ana
	Anic
	2013.
	Sisak
	kraul
	100
	1:00:54
	Majda
	Majdic
	2012.
	ZitoOS
	kraul
	100
	1:00:03
	Ivana
	Horvat
	2012.
	ZitoOS
	kraul
	100
	1:00:33


	Omogućiti korisniku unos podataka za željeni broj osoba (max. 20) u polje struktura.
	Ispisati osobu koja je u disciplini kraul, 100 m imala najbolje ostvareno vrijeme na način:

	REZULTATI:
	1. mjesto: Majda Majdic u disciplini kraul, 100 m.

	Deklaracije funkcije strcmp je: int strcmp (const char* str1, const char* str2); i vraca 0 ako su stringovi jednaki*/

int main(void)
{
	int brojPlivaca;
	scanf("%d", &brojPlivaca);
	PLIVAC *Plivac = malloc(sizeof(PLIVAC) * brojPlivaca);
	for (int i = 0; i < brojPlivaca; i++)
	{
		char ime[20], prezime[20], imeKluba[20], disciplina[20];
		int godRodenja, duzina;
		scanf("%s", Plivac[i].ime);
		scanf("%s", Plivac[i].prezime);
		scanf("%d.", &Plivac[i].godRodenja);
		scanf("%s", imeKluba);
		scanf("%s", Plivac[i].disciplina);
		scanf("%dm", &Plivac[i].duzina);
		scanf("%d:%d:%d", &Plivac[i].minuta, &Plivac[i].sekunda, &Plivac[i].milisekunda);
	}

	int najboljeVrijeme[3]={99,59,99};
	for(int i=0;i<brojPlivaca;i++){
		int najboljeVrijemee[3]=vratiNajboljeVrijeme(Plivac[i].minuta,Plivac[i].sekunda,Plivac[i].milisekunda,najboljeVrijemee);
	}

	return 0;
}