/*
Napisati C program koji koristi jednostruko povezani popis za strukturu COVID.

Struktura ima pokazivace na ime, prezime i pokazivac na strukturu DETALJI,
koja ima clanove: broj kontakata, ocjena razine simptoma [od 1 do 10], cijepljeni [da - 1, ne - 0].

Napisati funkcije za kreiranje povezanog popisa, umetanje novog clana na pocetak popisa,
pretraživanje popisa po broju kontakta, ispis popisa.

Napisati funkciju unosOsobe() koja ima pokazivac na strukturu COVID s kojom se unose podaci u clanove strukture.

Napisati funkciju ispisOsobe() koja ispiuje sve osobe.

Napisati funkciju pretragaPopisa() za pretrazivanje popisa
koji ima zeljeni broj kontakta.

Napisati funkciju ispisZeljeneOsobe() koja ce ispisivati nadjenu osobu u popisu.

Napisati funkciju oslobadjanjeOsoba() koja oslobađa dinamički zauzetu memoriju
za povezani popis.

                                    *****************************
                                    *****************************
                                    *****IZNIMNO UPOZORENJE******
                                    ***********SLIJEDI***********
                                    *****************************
                                    *****************************
                                    **********NAPOMENA***********
                                    *****************************
                                    *****************************
                                    *****IZNIMNO UPOZORENJE******
                                    ***********SLIJEDI***********
                                    *****************************
                                    *****************************
                                        
Iako vam testovi mogu imati 100/100, pregledavat će se jeste li radili provjeru nakon svakog dinamičko zauzimanja memorije kao i oslobađanje memorije.
Ako bi se dogodilo da to niste odradili, npr. provjera nije odrađena u prvom dijelu, 
tada mozete dobiti pola bodova ako vam drugi dio zadatka radi, tj. ako ste ujedno oslobodili i memoriju. Vrijedi i obrnuto, ako imate provjere,
a niste sve oslobodili, tada možete dobiti samo pola bodova. Molim vas da obratite pozornost na ove zahtjeve!!!
*/


#include <stdio.h>
#include "functions.h"

int main() {

	int n, i, x;

	COVID* pocetakListe = NULL;
	COVID* osobaKontakt = NULL;

	printf("Koliko osoba zelis upisati u registar COVID osoba?\n");
	scanf("%d", &n);

	pocetakListe = kreiranjeCovidListe();
	upisOsobe(pocetakListe);
	for (i = 0; i < n - 1; i++)
	{
		pocetakListe = ubaciNoviCvorUListu(pocetakListe);
		upisOsobe(pocetakListe);
	}

	ispisOsoba(pocetakListe);//naravno da ce ovo bit prazno xd
	printf("Osoba s koliko kontakta?\n");
	scanf("%d", &x);
	osobaKontakt = pretragaPopisa(pocetakListe, x);
	ispisZeljeneOsobe(osobaKontakt);
	printf("REZULTATI:\n");
	if (osobaKontakt == NULL) {
		printf("Osoba s toliko kontakata nije pronadjena!");
	}
	else {
		ispisZeljeneOsobe(osobaKontakt);
	}

	pocetakListe = oslobadjanjeOsoba(pocetakListe);
	printf("\nOslobodjen cvor!\n");

	return 0;
}