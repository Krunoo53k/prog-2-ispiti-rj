#include <stdlib.h>
#include <stdio.h>
#include "functions.h"

void upisOsobe(COVID *elementListe)
{
    // zauzmi malo za ime-----------napisano
    // malo to provjeri
    // zauzmi malo za prezime
    // malo to provjeri
    scanf("%s", elementListe->ime);
    scanf("%s", elementListe->prezime);
    scanf("%d", &elementListe->osoba->brKontakti);
    scanf("%d", &elementListe->osoba->ocjena);
    scanf("%d", &elementListe->osoba->cijepljeni);

    // dohvati ime
    // dohvati prezime
    // zauzmi malo za osobu
    // malo to provjeri
    // dohvati broj kontakata
    // dohvati ocjenu
    // dohvati cijepljeni
}

COVID *kreiranjeCovidListe()
{

    // dovrsi implementaciju
    COVID *pocetakListe = calloc(1, sizeof(COVID));
    printf("Kreiranje covid liste!\n");
    if (pocetakListe == NULL)
    {
        perror("Kreiranje");
        return NULL;
    }
    else
    {
        pocetakListe->ime = (char *)malloc(50);
        if (pocetakListe->ime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        pocetakListe->prezime = (char *)malloc(50);
        if (pocetakListe->prezime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        pocetakListe->osoba = (DETALJI *)malloc(sizeof(DETALJI));
        if (pocetakListe->osoba == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        pocetakListe->noviPacijent = NULL;
    }
    return pocetakListe;
}

COVID *ubaciNoviCvorUListu(COVID *pocetakListe)
{

    // dovrsi implementaciju
    COVID *noviPocetakListe = (COVID *)calloc(1, sizeof(COVID));
    if (noviPocetakListe == NULL)
    {
        perror("Kreiranje");
        return NULL;
    }
    else
    {
        noviPocetakListe->ime = (char *)malloc(50);
        if (noviPocetakListe->ime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        noviPocetakListe->prezime = (char *)malloc(50);
        if (noviPocetakListe->prezime == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }
        noviPocetakListe->osoba = (DETALJI *)malloc(sizeof(DETALJI));
        if (noviPocetakListe->osoba == NULL)
        {
            perror("Kreiranje");
            return NULL;
        }

        kreiranjeCovidListe(noviPocetakListe);
        noviPocetakListe->noviPacijent = pocetakListe;
    }
    return noviPocetakListe;
}

void ispisOsoba(COVID *obilazakListe)
{
    // dovrsi implementaciju-----napisano
    if (obilazakListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    else
    {
        while (obilazakListe)
        {
            printf("Ime: %s\nPrezime:%s\nBroj kontakata:%d\n", obilazakListe->ime, obilazakListe->prezime, obilazakListe->osoba->brKontakti);
            obilazakListe = obilazakListe->noviPacijent;
        }
    }
}

COVID *pretragaPopisa(COVID *pocetakListe, int brojKontakata)
{

    // dovrsi implementaciju
    if (pocetakListe == NULL)
    {
        return NULL;
    }
    else
    {
        while (pocetakListe)
        {
            if (pocetakListe->osoba->brKontakti == brojKontakata)
                return pocetakListe;
            pocetakListe = pocetakListe->noviPacijent;
        }
    }
    return NULL;
}

void ispisZeljeneOsobe(COVID *obilazakListe)
{
    // dovrsi implementaciju-----napisano
    if (obilazakListe != NULL)
    {
        printf("Ime: %s\nPrezime:%s\nBroj kontakata:%d\n", obilazakListe->ime, obilazakListe->prezime, obilazakListe->osoba->brKontakti);
    }
}

COVID *oslobadjanjeOsoba(COVID *pocetakListe)
{
    COVID *elementZaBrisanje = NULL;

    // dovrsi implementaciju
    // obrisi sve za što se din. zauzela memorija!
    elementZaBrisanje = pocetakListe;
    while (pocetakListe)
    {
        pocetakListe=pocetakListe->noviPacijent;
        free(elementZaBrisanje->osoba);
        free(elementZaBrisanje->prezime);
        free(elementZaBrisanje->ime);
        free(elementZaBrisanje);
        elementZaBrisanje=pocetakListe;
    }
    return NULL;
}
