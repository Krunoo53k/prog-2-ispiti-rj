#ifndef FUNCTIONS_H
#define FUNCTIONS_H

typedef struct detalji {
	unsigned int brKontakti;
	unsigned int ocjena;
	unsigned int cijepljeni;
}DETALJI;

typedef struct covid {
	char *ime;
	char *prezime;
	DETALJI *osoba;
	struct covid* noviPacijent;
}COVID;

COVID* kreiranjeCovidListe();
COVID* ubaciNoviCvorUListu(COVID*);
void ispisOsoba(COVID*);
void upisOsobe(COVID*);
COVID* pretragaPopisa(COVID*, int);
void ispisZeljeneOsobe(COVID*);
COVID* oslobadjanjeOsoba(COVID*);

#endif //FUNCTIONS_H