/*
Napisati C program koji koristi jednostruko povezani popis za strukturu TELEVIZOR.

Struktura ima varijable vrsta televizora [0 - LCD, 1 - LED, 2 - OLED], dijagonala [43, 65, 100]
i pokazivac na strukturu PROIZVODJAC, koja ima clanove: pokazivace na ime i drzavu prozivodnje,
i cjelobrojnu varijablu ocjena koja odgovara ocjeni brzine isporuke [od 1 do 10].

Napisati odgovarajuce funkcije za kreiranje povezanog popisa i ubacivanje novog clana:
kreiranjePopisaTelevizora(), ubaciNoviTelevizor().

Napisati funkciju upisTelevizora() s kojom se unose svi podaci u clanove strukture i pozvati
funkciju na odgovarajućem mjestu u programu.

Napisati funkciju ispisTelevizora() koja ispisuje povezani popis u obliku:

Vrsta televizora:
Ime proizvodjaca:
Ocjena brzine dostave:

Napisati funkciju pretragaPopisaTelevizora() za pretrazivanje povezanog popisa po zeljenoj ocjeni brzine
isporuke.

Napisati funkciju ispisZeljenogTelevizora() koja ispisuje pronađeni televizor prethodnom funkcijom.
Ako ih ima više takvih, ispisati prvi u popisu. Ispis neka bude u obliku:

Vrsta televizora:
Dijagonala:
Ime proizvodjaca:
Drzava proizvodnje:

Napisati funkciju oslobadjanjeTelevizora() koja oslobadja dinamicki zauzetu memoriju
za povezani popis.

U potpunosti rukovati memorijom za sve varijable za koje je to nužno.

									*****************************
									*****************************
									*****IZNIMNO UPOZORENJE******
									***********SLIJEDI***********
									*****************************
									*****************************
									**********NAPOMENA***********
									*****************************
									*****************************
									*****IZNIMNO UPOZORENJE******
									***********SLIJEDI***********
									*****************************
									*****************************

Iako vam testovi mogu imati 100/100, pregledavat će se jeste li radili provjeru nakon svakog dinamičko zauzimanja memorije kao i oslobađanje memorije.
Ako bi se dogodilo da to niste odradili, npr. provjera nije odrađena u prvom dijelu,
tada mozete dobiti pola bodova ako vam drugi dio zadatka radi, tj. ako ste ujedno oslobodili i memoriju. Vrijedi i obrnuto, ako imate provjere,
a niste sve oslobodili, tada možete dobiti samo pola bodova. Molim vas da obratite pozornost na ove zahtjeve!!!
*/
#include <stdio.h>
#include "functions.h"

int main() {

	int brojTelevizora, i, ocjenaTelevizora;

	TELEVIZOR* popisTelevizora = NULL;
	TELEVIZOR* zeljeniTelevizora = NULL;


	brojTelevizora = unosBrojaTelevizora();
	popisTelevizora = kreiranjePopisaTelevizora();

	for (i = 0; i < brojTelevizora - 1; i++)
	{
		popisTelevizora = ubaciNoviTelevizor(popisTelevizora);
	}

	ispisTelevizora(popisTelevizora);
	ocjenaTelevizora = brojZaPretraguTelevizora();
	zeljeniTelevizora = pretragaPopisaTelevizora(popisTelevizora, ocjenaTelevizora);
	printf("REZULTATI:\n");

	if (zeljeniTelevizora == NULL) {
		printf("Televizor s tom ocjenom nije pronadjen!");
	}
	else {
		ispisZeljenogTelevizora(zeljeniTelevizora);
	}

	popisTelevizora = oslobadjanjeTelevizora(popisTelevizora);
	if (popisTelevizora == NULL)
		printf("Oslobodjen popis televizora!");

	return 0;
}