#ifndef HEADER_H
#define HEADER_H

typedef struct televizor {
	int vrsta;
	int dijagonala;
	struct proizvodjac* proizvodjac;
	struct televizor* noviCvor;
}TELEVIZOR;

typedef struct proizvodjac {
	char* ime;
	char* drzava;
	int ocjena;
}PROIZVODJAC;

int unosBrojaTelevizora();
int brojZaPretraguTelevizora();
void upisTelevizora(TELEVIZOR*);
void ispisTelevizora(TELEVIZOR*);
TELEVIZOR* kreiranjePopisaTelevizora();
TELEVIZOR* ubaciNoviTelevizor(TELEVIZOR*);
TELEVIZOR* pretragaPopisaTelevizora(TELEVIZOR*, int);
TELEVIZOR* oslobadjanjeTelevizora(TELEVIZOR*);
void ispisZeljenogTelevizora(TELEVIZOR*);
#endif //HEADER_H