#include <stdlib.h>
#include<stdio.h>
#include "functions.h"

int unosBrojaTelevizora() {
	int n;
	printf("Koliko televizora u popis zelis upisati?\n");
	scanf("%d", &n);

	return n;
}

TELEVIZOR* kreiranjePopisaTelevizora() {

//dovrsi implementaciju------napisano
     TELEVIZOR *headNode = (TELEVIZOR*)calloc(1, sizeof(TELEVIZOR));
    if (headNode == NULL) {
        perror("Kreiranje");
        return NULL;
    }
    else {
             headNode->proizvodjac = (PROIZVODJAC*)malloc(sizeof(PROIZVODJAC));
            if (headNode->proizvodjac == NULL) {
                perror("Kreiranje");
                return NULL;
            }
            headNode->proizvodjac->ime = (char*)malloc(50);
            if (headNode->proizvodjac->ime == NULL) {
                perror("Kreiranje");
                return NULL;
            }
             headNode->proizvodjac->drzava = (char*)malloc(50);
            if (headNode->proizvodjac->drzava == NULL) {
                perror("Kreiranje");
                return NULL;
            }
            upisTelevizora(headNode);
            headNode->noviCvor=NULL;
            
        
    }
   return headNode;
}

void upisTelevizora(TELEVIZOR* glavaPopisaTelevizora) {
    
//dovrsi implementaciju----------napisano
    scanf("%d", &glavaPopisaTelevizora->vrsta);
    scanf("%d", &glavaPopisaTelevizora->dijagonala);
    scanf("%s", glavaPopisaTelevizora->proizvodjac->ime);
    scanf("%s", glavaPopisaTelevizora->proizvodjac->drzava);
    scanf("%d", &glavaPopisaTelevizora->proizvodjac->ocjena);
}

TELEVIZOR* ubaciNoviTelevizor(TELEVIZOR* glavaPopisaTelevizora) {

	//dovrsi implementaciju-----napisano
	TELEVIZOR *newHeadNode = (TELEVIZOR*)calloc(1, sizeof(TELEVIZOR));
    if (newHeadNode == NULL) {
        perror("Kreiranje");
        return NULL;
    }
    else {
             newHeadNode->proizvodjac = (PROIZVODJAC*)malloc(sizeof(PROIZVODJAC));
            if (newHeadNode->proizvodjac == NULL) {
                perror("Kreiranje");
                return NULL;
            }
            newHeadNode->proizvodjac->ime = (char*)malloc(50);
            if (newHeadNode->proizvodjac->ime == NULL) {
                perror("Kreiranje");
                return NULL;
            }
             newHeadNode->proizvodjac->drzava = (char*)malloc(50);
            if (newHeadNode->proizvodjac->drzava == NULL) {
                perror("Kreiranje");
                return NULL;
            }
            upisTelevizora(newHeadNode);
            newHeadNode->noviCvor=glavaPopisaTelevizora;
            
        
    }
   return newHeadNode;
}

void ispisTelevizora(TELEVIZOR* iter) {

	//dovrsi implementaciju---------napisano
	if(iter==NULL){
	    exit (EXIT_FAILURE);
	}
	else{
	    while(iter){
	        printf("Vrsta televizora: %d\nIme proizvodjaca:%s\nOcjena brzine dostave:%d\n", iter->vrsta, iter->proizvodjac->ime, iter->proizvodjac->ocjena);
	        iter=iter->noviCvor;
	    }
	}
}

int brojZaPretraguTelevizora() {
	
	int ocjena;
	printf("Televizor s kojom ocjenom proizvodjaca zelis?\n");
	scanf("%d", &ocjena);
	return ocjena;
}

TELEVIZOR* pretragaPopisaTelevizora(TELEVIZOR* glavaPopisaTelevizora, int ocjena) {

	//dovrsi implementaciju
	if (glavaPopisaTelevizora == NULL) {
        return NULL;
    }
    else {
        while (glavaPopisaTelevizora) {
            if(glavaPopisaTelevizora->proizvodjac->ocjena==ocjena)
            return glavaPopisaTelevizora;
            glavaPopisaTelevizora=glavaPopisaTelevizora->noviCvor;
        }
    }
	
}

void ispisZeljenogTelevizora(TELEVIZOR* zeljeniTelevizor) {

	//dovrsi implementaciju--------------napisano
	if(zeljeniTelevizor!=NULL){
	    printf("Vrsta televizora: %d\nDijagonala:%d\nIme proizvodjaca:%s\nDrzava proizvodnje:%s\n",zeljeniTelevizor->vrsta, zeljeniTelevizor->dijagonala, 
	    zeljeniTelevizor->proizvodjac->ime, zeljeniTelevizor->proizvodjac->drzava);
	}
}

TELEVIZOR* oslobadjanjeTelevizora(TELEVIZOR* iter) {

	TELEVIZOR* tmp = NULL;
    tmp=iter;
    iter=iter->noviCvor;
    free(iter->proizvodjac);
    free(iter->proizvodjac->ime);
    free(iter->proizvodjac->drzava);
    free(iter);
  //dovrsi implementaciju
}












